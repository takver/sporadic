import Vue from 'vue'
import VueI18n from 'vue-i18n'
import moment from 'moment'
import activityAPI from '@/services/api/activity'
import profileAPI from '@/services/api/profile'

// TODO - consolidate different custom emoji rendering methods into one
// there's an embarassing amount of too many of them right now.
// Profile, Status, Usernames in Status, Replies, Usernames in Replies
// not totally sure on how to consolidate yet.

Vue.mixin({
  methods: {
    niceFormat (content) {
      return moment(String(content)).format('MM/DD/YYYY hh:mm')
    },
    fuzzyFormat (content) {
      return moment(String(content)).fromNow()
    },
    format_handle (content) {
      content = content.replace(/^https?:\/\//, '')
      return content
    },
    emojify (content) {
      return content
    },
    statusEmoji (content) {
      if (this.activity.reblog) {
        var emojoList = this.activity.reblog.emojis
      } else {
        emojoList = this.activity.emojis
      }
      emojoList.forEach((emoji) => {
        let shortcode = ':' + emoji.shortcode + ':'
        let emojoURL = emoji.static_url
        let emojo = `<img src="${emojoURL}" class="emojo" alt="${shortcode}" title="${shortcode}"></img>`
        content = content.replaceAll(shortcode, emojo)
      })
      return content
    },
    contextEmoji (content) {
      var emojoList = this.context.emojis
      emojoList.forEach((emoji) => {
        let shortcode = ':' + emoji.shortcode + ':'
        let emojoURL = emoji.static_url
        let emojo = `<img src="${emojoURL}" class="emojo" alt="${shortcode}" title="${shortcode}"></img>`
        content = content.replaceAll(shortcode, emojo)
      })
      return content
    },
    profileEmoji (content) {
      let emojoList = this.account.emojis
      emojoList.forEach((emoji) => {
        let shortcode = ':' + emoji.shortcode + ':'
        let emojoURL = emoji.static_url
        let emojo = `<img src="${emojoURL}" class="emojo" alt="${shortcode}" title="${shortcode}"></img>`
        content = content.replaceAll(shortcode, emojo)
      })
      return content
    },
    accountEmoji (content) {
      if (this.activity.reblog) {
        var emojoList = this.activity.reblog.account.emojis
      } else {
        emojoList = this.activity.account.emojis
      }
      emojoList.forEach((emoji) => {
        let shortcode = ':' + emoji.shortcode + ':'
        let emojoURL = emoji.static_url
        let emojo = `<img src="${emojoURL}" class="emojo" alt="${shortcode}" title="${shortcode}"></img>`
        content = content.replaceAll(shortcode, emojo)
      })
      return content
    },
    remoteEmoji (content) {
      var emojoList = this.context.account.emojis
      emojoList.forEach((emoji) => {
        let shortcode = ':' + emoji.shortcode + ':'
        let emojoURL = emoji.static_url
        let emojo = `<img src="${emojoURL}" class="emojo" alt="${shortcode}" title="${shortcode}"></img>`
        content = content.replaceAll(shortcode, emojo)
      })
      return content
    }
  }
})
