import axios from 'axios'
import store from '@/store'
const ACCOUNTURL = 'api/v1/accounts'
var BASEURL

export default {
  name: 'profileAPI',
  configure (cfg) {
    BASEURL = cfg.BASEURL || ''
  },
  getBio (id) {
    return axios({
      method: 'GET',
      url: `${BASEURL}/${ACCOUNTURL}/` + id
    })
  },
  getActivities (id) {
    return axios({
      method: 'GET',
      url: `${BASEURL}/${ACCOUNTURL}/` + id + '/statuses',
      params: {
        'limit': '150'
      }
    })
  },
  getRelationship (id) {
    return axios({
      method: 'GET',
      url: `${BASEURL}/${ACCOUNTURL}/relationships`,
      params: {
        id: id
      }
    })
  }
}
